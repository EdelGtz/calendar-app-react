const {response} = require('express');
const bcrypt = require('bcryptjs');
const Usuarios = require('../models/Usuarios');
const { generarJWT } = require('../helpers/jwt');

const crearUsuario = async(req, res = response) => {
    const { email, password } = req.body;

    try {
        // Validar que no exista un usuario igual
        let usuario = await Usuarios.findOne({ email });
        if ( usuario ) {
            return res.status(400).json({
                ok: false,
                msg: 'Un usuario existe con ese correo',
            })
        }

        usuario = new Usuarios( req.body );    

        // Encriptar contraseña
        const salt = bcrypt.genSaltSync();
        usuario.password = bcrypt.hashSync( password, salt );

        // Guardar registro
        await usuario.save();

        // Generar JWT
        const token = await generarJWT( usuario.id, usuario.name );
    
        res.status(201).json({
            ok: true,
            uid: usuario.id,
            name: usuario.name,
            token
        });     

    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Por favor hable con el administrador',
        });
    }

}

const loginUsuario = async(req, res = response) => {
    const { email, password } = req.body; 
    
    try {
        // Validar que no exista un usuario igual
        const usuario = await Usuarios.findOne({ email });
        if ( !usuario ) {
            return res.status(400).json({
                ok: false,
                msg: 'El usuario o contraseña son incorrectos',
            })
        }

        // Confirmar passwords
        const validPassword = bcrypt.compareSync( password, usuario.password );
        if ( !validPassword) {
            return res.status(400).json({
                ok: false,
                msg: 'Password incorrecto'
            });            
        }

        // Generar JWT
        const token = await generarJWT( usuario.id, usuario.name );

        res.json({
            ok: true,
            uid: usuario.id,
            name: usuario.name,
            token
        })

    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Por favor hable con el administrador',
        });
    }
}

const revalidarToken = async(req, res = response) => {

    const { uid, name } = req;

    // Generar nuevo JWT
    const token = await generarJWT( uid, name );

    res.json({
        ok: true,
        uid,
        name,
        token
    });
}




module.exports = {
    crearUsuario,
    loginUsuario,
    revalidarToken
}