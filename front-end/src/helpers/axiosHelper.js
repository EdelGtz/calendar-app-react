import axios from 'axios';

const baseUrl = process.env.REACT_APP_API_URL;


const fetchSinToken = ( endpoint, data, method = 'GET' ) => {
    const url = `${ baseUrl }/${ endpoint }`;

    const headers = {
        'Content-Type': 'application/json'
      }

    let dataToSend = JSON.stringify( data );

    if ( method === 'GET' ) {
        axios.get( url )
            .then(res => {
                return res.data;
            })
    } else if ( method === 'POST') {
        axios.post( url, dataToSend, { headers: headers } )
        .then(res => { 
            console.log(res)           
            return res.data;
        })
        .catch(err => {
            console.log(err.toJSON())
            return err;
        })
    }
}

export {
    fetchSinToken,
}